# Backup
##  Backup erstellen

```bash
sudo mkdir /backup
sudo chmod 777 -R /backup
cd /backup
ssh root@webprogrammieren.de mysqldump -u root -p --all-databases > /backup/sicherung.sql;
ssh root@webprogrammieren.de zip -r gitea.zip /gitea
scp  root@webprogrammieren.de:gitea.zip /backup
```
## Backup zurücksichern

### Datenbank zurücksichern

```bash
wget -c dev.mysql.com/get/mysql-apt-config_0.8.12-1_all.deb
apt-get install  gnupg 
sudo dpkg -i mysql-apt-config*
sudo apt update
sudo apt-get install mysql-server

mysql -u root -p < sicherung.sql

```