# Deskop
## ssh Schlüsssel erstellen
```bash
ssh-keygen -t rsa -b 4096 
ssh-copy-id -i ~/.ssh/id_rsa.pub user@server 

``` 
## Go Installieren
```bash
wget https://dl.google.com/go/go1.12.7.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.12.7.linux-amd64.tar.gz
sudo nano /etc/environment
------Datei----------
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
   ------------------
 reboot

``` 
## Mysql Workbench
### Vorher Installieren

* [libssl](https://packages.ubuntu.com/cosmic/amd64/libssl1.0.0/download)
* [libzip4](https://packages.ubuntu.com/cosmic/amd64/libzip4/download)

#### Installieren

* [Mysql Workbench](https://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-workbench-community_8.0.16-1ubuntu18.04_amd64.deb)

## Installieren wir Visual Studio Code
* [Visual Studio](https://code.visualstudio.com/)


