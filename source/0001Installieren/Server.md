# Ubuntu Server
## webprogrammieren.de
### certbot Installieren
*  [Cerbort](https://certbot.eff.org/)
### MYSQL 8

``` bash
   wget -c https://dev.mysql.com/get/mysql-apt-config_0.8.13-1_all.deb
   apt-get install  gnupg 
   sudo dpkg -i mysql-apt-config*
   sudo apt update
   sudo apt-get install mysql-server
   sudo systemctl enable mysql
   sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf
   -------------------Datei mysqld.cnf --------------
   [mysqld]
   bind-address = 0.0.0.0
   -------------------Datei mysqd ende----------------
   mysql -u root -p
   use mysql;
      update user set host='%' where user='eigene Benutzername';
      update db set host='%' where user='euer_benutzer';
      CREATE DATABASE git;
      CREATE DATABASE webprogrammieren.de;
      exit;
      sudo service mysql restart
```
### Git

```bash   
sudo apt-get install git-core
sudo apt update
sudo apt install git
```
### Gitea

```bash
wget -O gitea  https://dl.gitea.io/gitea/1.8/gitea-1.8-linux-amd64
chmod +x gitea
```
