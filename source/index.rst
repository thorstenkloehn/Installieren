.. Sphinx documentation master file, created by
   sphinx-quickstart on Mon Jun 17 13:03:05 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Webprogrammieren Dokument
=========================

.. toctree::
   :maxdepth: 4

   0001Installieren/index.rst
   0002Backup/Backup.md
   0003Upgrade/Upgrade.md
   Server/Server.md
   Impressum.md
   Datenschutzerklarrung.md
   
   


   





